This diary file is written by Keilin Chiu F74065050 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-18 #

* I wrote my success story on how AI is used in wildlife conservation. 
* I found it very interesting on how AI can easily outperform a human in so many situations.
* By reading other peoples success story ppts, I see how AI can save time and human resources, by doing jobs more effectively.
* I feel inspired now to learn about AI and machine learning in hopes to impliment it in ways that can help people in the future. 