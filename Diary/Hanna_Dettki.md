



This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-21

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* The first class was great!
* In my opinion, the topic of AI and its relevance was motivated in a great and vivid way!
* I wonder how far we are from general AI.
* I am looking forward to implementing my own Neural Network.
* I wonder who will be in my group.
* I am looking forward to getting to know to new people and making new friends.


# 2019-02-28

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I liked the overview given about different domains all relating to AI
* However, I still wonder what exactly the difference is between Data Mining and Data Science
* I think being able to extract information from huge amount of data and making sense of it is a pretty powerful skill
* I really want to aquire this skill
* I wonder how much data can be found about me on the internet and how much can be inferred from my data footprint


# 2019-03-06

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* It was really helpful to me that the professor clarified how the layout of a diary entry is supposed to look like
* I actually believe that the Readme_Diary file provides enough information except for the fact that to me it was not clear at first that there is only supposed to be one diary file and that you update it every week. I thought there is supposed a separated diary file for each week.
* I believe it is really convenient that Python does not require to define data types
* I think Python is a great programming language and I am looking forward to learning it.


# 2019-03-13

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I liked that we got to practice in class. I am not used to this from my German university.
* The CO-site provided gives a great overview of Python.
* I like that we get to exercise in some cells.
* I think Python is great and I am looking forward to applying Python to Neural Networks.
* However, I am still a bit confused about the self argument used in functions.

#2019-03-20

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I find it hard to grasp that there are so many different possibilities of how a Neural Networks' biases and weights can be set.
* I find it quite amazing that usually, the function that maps the input to the output values produces some reasonable output by having found good values for the weights and bias values.
* I wonder how much time it would take to calculate all the values regarding weights and biases of a Network which has a 28*28 pixel input having 12 layers, just as presesented in the 3 Blue 1 Brown youtube video. I wonder whether the calculation by hand would find a function approximating the output at least as good as the neural network does.
<<<<<<< HEAD
* I like Yann LeCun's illustration of a cake depicting the amount of informatin predicted by a neural network 
* I like the way the professor said training a NN is like art


#2019-03-27

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I wonder what other metrics there are to evaluate an NN's accuracy than counting true positives and true positives
* I wonder whether there is a systematic way to figure out how many layers an NN should include
* I think it is fascinating that so much of an NN's output relies on the activation function, such as relu, hyperbolic tangens,sigmoid,  ...
* I believe it is quite philosophycal to think about reality and how one can depict reality and the real system in models. When the model actually depicts the reality down to every single particle, we don't have a model anymore but a copy of reality.
* I would like to know more about backpropagation, I only kind of grasped the idea that it is about a lot of calculus.    
=======
* I like Yann LeCun's illustration of a cake depicting the amount of informatin predicted by a neural network
* I like the way the professor said training a NN is like art    
>>>>>>> 67a977d86b464e55f533d0a8053cff6193c1462d

#2019-04-10

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I am surprised by finding the global optimum of the loss function is actually not desirable. I did not know that finding the global optimum is equal to ovefitting the model.
* I am quite stunned that Convolutional neural networks are inspired by the eye, being receptive fields of the retina to filters of the network
* I need to look up what Maxwell's equation calcaulates. It is much easier to sove using tensor algebra than using vector algebra
* I believer there so many good grasping methods used with CNNs such as zero padding in order to account for object detection where the obeject is actually not visible in its whole but only a part of it
* I cannot yet grasp the fact that a CNN is a special case of a fully connected network
* Now I know why with CNNs one bothers to define lots of little small networks: It is in order to account for avoiding overfitting and providing better generalization

#2019-04-17

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I am quite surprised, that there are only 7 steps required in order to train a Deep Neural Network 
* I wonder how far we are from General AI since current NNs are still narrow AI
* I am quite thankful that there are so many good tools provided out there in order to make making predictions via deep learning fairly easy
* I wonder what Transfer Learing is and how it can be applied 
* I like the idea the teacher mentioned that deep learning is democrocizing
* I am looking forward to learning how to do a webservice with python 

#2019-04-24

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I liked that we had a practice session today in order to get familiar with tensor flow and in particular keras 
* I still do not really get what Keras is exactly. As far as I understand it is some high level library making use of TensorFlow and thereby facilitating the process of building an NN but I need to look it up 
* I still believe google colabs is pretty cool and it is great that google provides it to the public 
* I am excited about building a web service using flask

#2019-05-01

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I don't think DL is hard in terms of implementation since Keras and TF is basically doing all the hard work
* However, I think Dl is really hard when it comes to understanding all the math behind a NN.
* I mean it is always possible to try to minimize the loss function by applying  different optimerzers and loss functions by making use of trial and error.
* Hence, I would really love to learn more about all the theory behind NNs in order to being able to do DL systematically. 
* I liked that the class involved some practice and demanded us to participate again 

#2019-05-08

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I found it really interesting to learn about what tensors really are
* when the professor breaks down the model of the network it looks quite simple but ai know thwt there is a lot of math behind the model which is hidden in sll the libraries being imported st the beginnging of the source code. 
* I winder how transfer learning really works
* I like the webservice since it is a great way to share work and knowledge
* suppossedly, networks are based on supervised and reinforcement learning. i kind of forgot how they work. I need to look it up again.

#2019-05-15

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I am surprised about the fact that Saudi Arabia is subsidising fosil base energy more heavily than all accumulated subsidies for renewable energies
* I agree with the professor that we are currently being  in the third and not fourth industrial revolution as Siemens claims
* I am quite surprised about the fact of how many jobs can be automated in the short time frame of two years
* AI still lacks a lot in creativity. So, I like the idea put forward by the professor of going into some creative job and using AI as your supporter

#2019-05-22

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I found the TED Talk really inspiring. It really is up to us on deciding how AI is going to turn out
* I find it quite stunning that in the field of image recognition there are quite huge gaps created by biases in the dataset resulting in validating for instance a burning house as beautiful and great.
* I believe the professor's example on how youtube's algorithm suggests movie is a great example of algorithm bias that we encounter in everyday life.
* I am quite stunned by the fact that more than 117 mio people have their faces in facial recognition software.
* I find the bias placed in predicitve policing quite criticial because it cuold easily create unfair outcomes
* I wonder whether we are paying the same prize for the same product on the same platform or whether this differs based on factors only the algorithm knows 

#2019-05-29

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I like that we finally get started on creating our own neural network
* However, I am kind of overwhelmed by the plethora of possibilities on how to choose the parameters
* I wonder what the accuracy of our network is going to be
* I think I should look up the different activation functions again, like the difference between relu and sigmoid because I think the activitation function is a crucial part contributing to the network's accuracy
* I think as a start, I should look up the state of the art way to do digit recognition 
* I think I should google how to add convolutional layers to a neural network

#2019-06-05

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I wonder why my code is throwing errors even though it seems to be right
* I think we still have a long way to go
* I wonder what accuracy our network will reach
* I am aming for 99.5 % accuracy

#2019-06-12

This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I regret that I forgot to show the graph of our model in tensorboard
* I learned a bit from the resources other groups presented 
* I think it is quite amazing how much erverybody learned during the semester
* I definetely want to commit myself more to Deep Learning and want to learn everything that there is to learn about it


#2019-06-19
This diary file is written by Hanna Dettki F74078443 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

* I did not like some questions of the exam. Particularly regarding the questions inquiring about some numbers and countries represented in the numerous statistics in the slides represented in the lecture. 
* I think google is there to look up such numbers and it is not necessary to remember them that detailed. In my opinion, it is enough to know about the rough trend and tendency.
* Generally, I still think that I learned a lot from this lecture.
* I think the goup presentations are much better this week compared to last week which is propably due to the fact that they now by now what features the grade depends on by having seen the survey already last week. 
