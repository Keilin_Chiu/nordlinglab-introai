This diary file is written by Chungshun Lin AN4056027 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

#2019-02-20
1. It is my first time to listen other success stories where AI outperform humans except AlphaGO.
2. I think AI is a promising technology to help people no matter in the academia or in daily life.
3. I wonder how phramacists perform their value and knowledge in a medical team and interact with patients when AI outperform than human.
4. I feel nervous about the project because I am not know much about data science, programming and so on.

#2019-02-27
1. I get some information about stastics, data science, AI, DL, ML.
2. Is it easy for me to establish an AI by meself?
3. Learning is a critical characteristic for AI and why we are amazed by them.
4. Is it possible for AI to learn emotion? (like Baymax in Big Hero 6)

#2019-03-06
1. After professor explained the rule of keeping diary, I think I know about how to do it without breaking the rules.
2. I spent a few time to install python with Junypter, and I think it wiil be good to give us a guidline to install python and Junypter by ourselves at home before today's class!
3. Print is a simple function to show what I write between the quotes.
4. "#" this in python means comment, and I can use this symbol to explain what my code means.
5. Backslash is used to escape some symbols like backflash, quotes, and so on.
6. Three quotes can ignore backslash and quotes in the sequence.
7. Conditional statements and loops can be used to a condition to do something again and again.
8. We can define our own functions and variables in python, and variables have two kinds: boolean and integer.

#2019-03-13
1. Today I did some exercises which was provided with a classmate in class.
2. I'm really agree with learning by doing, especially programming!
3. Even though I know some functions, it is a challenge to combine them to deal with exercises.

#2019-03-20
1. Neural network is a term used to computer systems to mimic animal neural system.
2. By using neural network, computer system is able to establish a model from labeled or unlabeled information.
3. Via established model, computer could do prediction or decision with lowest risk.
4. I learned the definition of classificaion and regression and where to utilize.

#2019-03-27
1. All models are not true systems because we couldn't distinguish model from true system if model is same with true data.
2. I like the idea of using Heisenberg's uncertain principle to explain why we only do approximator but not reach to ture system.
3. If the model is appproximated to true system enough, we don't need to calculate other complicated model.
4. How do we know the model is overfitting or underfitting?

#2019-04-10
1. Stochastic gradient descent (SGD) is a method to get the fittest function or model by mini-batching.
2. One of the cons of SCD is that learning rate should be appropriated ; otherwise, the global minimum of the loss function will not be found.  
<<<<<<< HEAD
3. Filtering is to pick useful information from data.

#2019-04-17
1. I learned a little about Tensorflow from today's lecture.
2. If we trained the Tensorflow with certain data, the Tensorflow can not distinguish other things we didn't train it before.
3. For example, if we train the Tensorflow how to recognize digits, we can not use them to recognize faces of human.
4. It looks complicated to program Tensorflow to do deep learning.
5. I need to read more supporting information for next lecture.

#2019-04-24
1. I was very confused in today's class.
2. I couldn't run the programm on my computer and I didn't how to solve it.
3. I tried to ask teaching assistants for help but he just said maybe so many people use it at the same time that it dysfunctioned.
4. However, I can't run the program even now I am in my dorm.
5. Hope next week I can finish the exercise.


#2019-05-01
1. I understood more about Tensorflow this week.
2. Teachers explained how to do exercises and let me learn a lot.
3. I think deep learning is hard because I still not really understand each step and why we program like this instead of other ways.
=======
3. Filtering is to pick useful information from data.
>>>>>>> origin/chungshunlin/chungshun_linmd-edited-online-with-bitbu-1554897826843
