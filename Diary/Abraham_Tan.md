This diary file is written by Abraham Tan C24057013 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #
* The lecture was informative.
* I am amazed at how much experience the lecturer had on dealing with data in all kinds of fields.
* It's amazing and scary how AlphaGo Zero can reach the limit of Go in just 40 days.
* I wonder if video games would be more creative and unpredictable in the future when everyone is able to program their 'players' to play for them.
* I think that over reliance on AI to predict something might cause error due to some overlooked mechanisms to be attributed to the imperfection of the AI.
* Although we often hear that AI is successful, I think we still need to be cautious on how we implement it.

# 2019-02-27 #
* The lecture gave me a general idea of each field, but it left me thinking about the similarities and differences of each field at the end.
* It is interesting how each field evolved independently and still could be used together to solve problems.
* Do system analysts use simulations or big data or both for their analysis of a particular system?
* Based on information found by googling, it seems that the only difference between AI and ML is that AI uses ML to make accurate decisions.
* I have watched the interview of Linus Torvalds as recommended by the teacher and it appears that I have him to thank for for my Android phone and the Internet in general. Wow, it's amazing what people can do working together in open source.
* It's a shame that we don't get to hear from the other participants about their assignments.

# 2019-03-06 #
* I really need to learn how to merge the editted files in bitbucket in the near future, and perhaps adding the steps to do so in the readme file also. My current method is to keep commiting the file until it works without error.
* I have not learn about the class and inheritence before. I do know about the other stuff though.
* I am excited to find out the application of dictionary and inheritence in AI.
* The videos that I had watched for learning class and inheritence are part 23 to 29 of this [Python tutorial for beginners series](https://www.youtube.com/watch?v=QOryrmkr1tA&index=52&list=PLS1QulWo1RIaJECMeUT4LFwJ-ghgoSH6n) and [an overview video of class and inheritance](https://www.youtube.com/watch?v=GY330Pgi18c).
* Looking at the reference, I found an interesting video by Python Codex on [sending an email with Gmail through Python](https://www.youtube.com/watch?v=IYhKdvfPas0&list=PL1Bjrbx1Tsx3TELwcMDRlxVjR8uv47o17&index=16). I'm going to try it this weekend.

# 2019-03-13 #
* Update on learning bitbucket: I have learned all the basic commands of Git, but I still yet to understand how to connect my local repository to the online shared one (nordlinglab-introai). It seems too complicated than it's worth, so I will leave this until the need arises.
* I have already read through and finished the exercises last week, so I taught my partner the underlying logic for the codes today.
* The Jupyter notebook looks very well structured and detail. I will spend more time with it during the weekends.
* I wonder when will I get to know who is my teammate. Though the group project will not start yet, we could help each other as a group to learn in class. That said, the pairing system used today is very effective for learning.
* I have done the MNIST digit regonizer project before with 90+% accuracy, though I do not actually understand most of the code used. I look forward to learning it again :D.

# 2019-03-20 #
* I have actually watched that particular 3 blue 1 brown video long ago. Surprisingly, I still remember most of the video.
* It seems like supervised learning would be the optimum choice when we have labelled data, and unsupervised learning when we only have unlabelled data.
* I wonder what is the functions of the other network types (CNN, GAN, DQN, ...).
* It seems like we students should just copy the hyperparameters used by others to skip the trial and error phase, and maybe play around with it to optimize the result to a specific data set.

# 2019-03-27 #
* I think that all models are 'wrong' because all models must be simplified to produce a statically correct answer that can be computed. This simplifying would exclude most underlying uncertainties that would affect the sample. There would always exist more uncertainties ignored in all models no matter how much you add on unless you included everything in the universe in the model.
* The philosophical answer indicates that a perfect moel would compute all uncertainties for all energy and matter in the universe for it to remain correct. This requires a way to record and measure all of these data, which would be impossible.
* I think it's cool that we can force the neural network to detect the features of the data by forcing the network to shrink at certain layers. It gives the system meanings we human can understand and further investigate.
* I have watched the 3 blue 1 brown video, and the concept of backpropagading being a application of gradient descent is amazing, yet easy to understand.
* Hopefully, I would be able to use the spring break productively and learn more neural network and artificial intelligent.

# 2019-04-10 #
* It's important to note that the various methods of finding the minimum is used to safe computing power and time for efficiency.
* Overfitting seemed to happen when a neural network is trained too well on a dataset.
* I wonder what is the general rule in guessing the optimum learning rate, if there is one at all.
* I get the logic behind making your own model instead of relying on the neural network in optimizing itself, but which type of data is 'making our own model' more efficient? I would guess one that you have very few training data on, as there's too little information for training.
* The concept of pooling seems awesome for finding patterns or features in a large data, helping us to gain new understanding on the data on hand.

# 2019-04-17 #
* I installed both keras and tensorflow successfully, but python idle could not run the code with error:"ModuleNotFoundError: No module named 'tensorflow.keras'".
* Due to next week being a test week, I would look into this problem after next week's rush is over.
* Diploying the model as a web service seems cool I wonder if it's possible for the service to save the image uploaded by the users?
* The coding seems simple, but making an effective model would take much more skills. The current method is by brute forcing, made possible with the current availability of the big mnist dataset. I wonder if making our own model will be as simple as this too?

# 2019-04-24 #
* Running the example given, I got an accuracy of 97% with validation accuracy of 94%. It seems this model is overtrained.
* Using the new parameters, I got an accuracy of 98.6% with validation accuracy of 97.2%. This model is well trained and produces high accuracy.
* Looking at the provided software installation tutorial, I need to activate the visualization function in my computer. I will try this after the tests.
* Because I have completed the exercises, I will try to use a more complicated model next week to achieve higher accuracy.

# 2019-05-01 #
* Since I have already completed the exercise, I now am left with the 'deploying web service' part, which seems to require me to run the code in my own device.
* I find the machine learning to be not difficult, but perhaps some example on how to import and analyze different kinds of data would be helpful. Below are some of the things I think would be great if we get a collab tutorials on.
* How to import any images and the codes to prepare it for the learning process (dimensions, normalization etc.)
* How to import audio clip and prepare it for the learning process.
* How to show the images in the black box layers to get a sense of what's going on in the machine learning (especially for the network with feature identification).

# 2019-05-08 #
* The teacher refreshes our memory on the definition and meaning of some key words such as 'backpropagation', 'neural network' and so on. I think these will be in the test.
* The teacher introduced some sources we can get articles on neural network and the like, but personally I prefer to google and search for tutorials as tutorials should be easier to understand.
* I think the questions asked by the students can be made into simple FAQ list rather than trying to explaining them all in class, due to the amount of questions there are.

# 2019-05-15 #
* While I understand automation would reduce the need of labor forces and thus money, I don't think it could help with enviromental problems.
* I wonder what is the best course of action we can take to make ourselves relevant to the future job market after this class ends.
* With AIs running chatbots and shops, would the future generations be less socialable and unable to work in groups?
* Would teaching coding and AI to students starting from middle school or earlier a must for the 3rd industrial revolution to happen?

# 2019-05-22 #
* Algorithm bias is a harder to remove than it seems. For facial regconition it might be a few hundreds more pictures in the training data set, but imagine algorithm bias for speech regconition for people with high pitched voice. That would requires hundreds of hours of their voice recordings, which would be difficult to solve if such bias were to exist.
* I think it is logical to use AI to detect fake videos made by AI, and it's viable to do so as making a fake video should in theory take more time than to detect it to be fake. Though, more people must be informed about this in case the fake videos becomes widespread one day.
* I personally like the way we can choose the difficulty of the final project ourselves. However, I think I will be content with a model that can predict up to 1% error or less, as long as the papers are not too difficult to understand. If everything fails, we will just have to stick with what we have and do things like proper engineers: trials and errors.

# 2019-05-29 #
* Today I met with my teammates and I was elected to be the leader of the group. I set the goal of that day's lesson to be to get used to playing with the model in google collab. Our highest validation accuracy was about 97%.
* I requested my teammates to try to play with the models more and search for relevant models online that we could refer to. I found this neat [article on the most effective CNN arhitecture for digit regconition](https://www.kaggle.com/cdeotte/how-to-choose-cnn-architecture-mnist). The model trained from this has a validation accuracy of at least 98%. However, the accuracy drops to 20% or so when I added dropout layers, unlike the increase seen in the article. I do not know why this happens.
* Now that we are building more complex models, it seems that more training is necessary for the model to preform well. I will try to ask the teaching assistants or the teacher about how to change these parameters and so on.
* For next week, my team and I will discuss on the various models we find online and try to choose a model to imitate.

# 2019-06-05 #
* We have chosen which model to make and running the training for the model for 60,000 pictures takes 7 hours. Hopefully we can finish it in time for the presentations without much problems.
* Due to the amount of time needed and the constant errors for "limit of buffered data reached", we will only run a epoch instead of the ideal 20+ epochs.
* We learned how to save and download the model to be used during the presentation.

# 2019-06-12 #
* Today we were told the criteria we are judged on by the other classmates just before the presentation. We were quite panicked and had to change some of things we planned to say at the last minute. It worked out in the end though.
* It is amazing that the other groups actually went through professional articles and got inspirations from them unlike us who uses a tutorial for the CNN MNIST dataset.
* I learned from the other groups presented today that there are so many ways to go about regconizing digits. However, I don't really understand how their models function as they could only give brief explainations.
* As the leader of group 6, I learnt the bare basics of operating the git bash to upload the files. Since everyone is probably busy with the finals, I volunteer to help them upload theirs if they so wished to.